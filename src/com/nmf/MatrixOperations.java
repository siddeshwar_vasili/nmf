package com.nmf;
class MatrixOperations 
{
	//Random number Generation for matrices
	 public static void randomize(Matrix A)
	{
        int rowsA = A.height;
        int colsA = A.width;
        float[] backingArrayA = A.values;
        for (int i=0; i < rowsA*colsA; i++) 
		{
            backingArrayA[i] = (float)java.lang.Math.random();
        }
	}

	//Matrix Multiplication
    public static Matrix matMultiply(Matrix A, Matrix W, Matrix H) 
   {
        int rowsA = W.height;
        int innerProdDim = W.width;
        int colsA = H.width;
		Matrix M1=new Matrix(rowsA,colsA);
        if ((A.height != rowsA) || (A.width != H.width) || (W.width != H.height)) 
		{
            throw new RuntimeException("Error: The matrices have inconsistant dimensions!");
        }
        float sum;
        float[] rowW = new float[innerProdDim];
        for (int i=0; i < rowsA; i++) 
		{
            for (int n=0; n < innerProdDim; n++) 
			{
                rowW[n] = W.get(i, n);
            }
            for (int j=0; j < colsA; j++) 
			{
				sum = 0;
				for (int m = 0; m < innerProdDim; m++) 
				{
					sum += rowW[m]*H.get(m, j);
				}
				M1.set(i,j, sum);
			}
		}
		return M1;
    }   


	//Finding Mean Squared Error Value
	public static double findMSE(Matrix A,Matrix M1)
	{
		 int rowsA = M1.height;
         int colsA = M1.width;
		 float res=0.0f;
		 float mse=0.0f;
		 float[] rowA = new float[rowsA];

		try{
					for (int i=0;i<rowsA;i++ )
					{
						for (int n=0; n < colsA; n++) 
						{
								rowA[n] = A.get(i, n);
						}

						for (int j=0; j < rowsA; j++) 
						{
								for (int m = 0; m < colsA; m++) 
								{
										res = res+(rowA[m]-M1.get(j, m))*(rowA[m]-M1.get(j, m));
								}
						}
					}
					mse=(1.0f/(rowsA*colsA))*res;
			}catch(Exception e)
			{
				System.out.println("A and Product matrices should have same order otherwise we cannot compute Mean Squared Error Value ...");
			}
	return mse;
	}
}