package com.nmf;
public class  Matrix
{

	 public int height;
     public int width;
     public float[] values;

	//Matrix Creation
	public Matrix(int rows, int cols) 
	{
        height = rows;
        width = cols;
        values = new float[rows*cols];
    }

	public void set(int row, int column, float val) 
	{
        values[row + height*column] = val;
    }

	/*public void set(int column, float val) 
	{
        values[column] = val;
    }*/

    public float get(int row, int column) 
	{
        return values[row + height*column];
    }

    public String toString() 
	{
        String out = "";
        for (int i = 0; i < height; i++) 
		{
            for (int j = 0; j < width; j++) 
			{
                out += get(i,j) + "   ";
            }
            out += "\n";
        }
        return out;
    }

}