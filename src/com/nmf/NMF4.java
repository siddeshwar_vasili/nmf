package com.nmf;
import java.util.Scanner;
public class  NMF4
{

	public static double mseOld;
	public static double mseNew;
	public static float diff;
	public static double temp;
	public static int choice;

	public static void main(String[] args) 
	{
		testNMF();
	}

	//Business Process Starts here
	public static void testNMF() 
	{

		Scanner sc=new Scanner(System.in);

		//Input for Matrix Rows, Columns and W,H matrix dimention value
		System.out.print("\nHow many particles ");
		 int rows = sc.nextInt(); 
		  System.out.print("\nHow many dimentions ");
		 int cols = sc.nextInt(); 
		System.out.println("Enter value for  Columns in W, Rows in H matrices: ");
		int cr=sc.nextInt();
		
		choice = PSO.mainMenu();
		//Matrices creation of A,W,H and insert random values in each matrix
        Matrix A = new Matrix(rows,cols);
        MatrixOperations.randomize(A);
        Matrix W = new Matrix(rows, cr);
		MatrixOperations.randomize(W);
        //W=PSO.returnW1(choice,rows,cr);
        Matrix H = new Matrix(cr, cols);
        MatrixOperations.randomize(H);

		//Matrix multiplication
		Matrix M1=MatrixOperations.matMultiply(A, W, H);
	    System.out.println("Matrix A Values:");
	    System.out.println(A);
		System.out.println();
		System.out.println("W and H Matrices product:");
		System.out.println(M1);
		
		//Checking matrices A and product of W,H equal or not
		if (A==M1)
			System.out.println("Given and Product Matrices are Same");
		else
			System.out.println("Given and Product Matrices are not Same");
	
		mseOld=MatrixOperations.findMSE(A,M1);		//Reading MSE value into variable
		int i=1;
				
				//Iterative process starts here each iteration Matrix A,H
		//are constants and matrix W values change
	   do{
				Matrix W1 = new Matrix(rows, cr);
				W1= PSO.returnW1(choice,rows,cr);
				Matrix M2=MatrixOperations.matMultiply(A, W1, H);
				System.out.println("Matrix A Values:");
				System.out.println(A);
				System.out.println();
				System.out.println("W and H Matrices product:");
				System.out.println(M2);
	
				//Checking matrices A and product of W,H equal or not
				System.out.println();
				if (A==M2)
					System.out.println("Given and Product Matrices are Same");
				else
					System.out.println("Given and Product Matrices are not Same");
				
				System.out.println();
				//Reading MSE value into variable
				mseNew=MatrixOperations.findMSE(A,M2);

				//Finding difference between MSE old and new values
				diff=(float)Math.abs(mseOld-mseNew);
				
				System.out.println("MSE old: "+mseOld);
				System.out.println("MSE new: "+mseNew);
				System.out.println("Difference: "+diff);

				temp=mseOld;
				mseOld=mseNew;

				System.out.println("Iteration : "+i);  //printing iterations
				System.out.println("************************************************");  

				i++;
		
			}while(diff>=0.0001);
			System.out.println("Given and Product Matrices are Approximately Same");
		}
}